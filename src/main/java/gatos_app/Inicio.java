package gatos_app;

import java.io.IOException;

import javax.swing.JOptionPane;

public class Inicio {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		int opcion_menu = -1;

		String[] botones = { "1. Ver gatos", "2. Salir" };

		do {

			String opcion = (String) JOptionPane.showInputDialog(null, "Gatos Java", "Menu Principal",
					JOptionPane.INFORMATION_MESSAGE, null, botones, botones[0]);

			for (int i = 0; i < botones.length; i++) {
				if (opcion.equals(botones[i])) {
					opcion_menu = i;
				}
			}

			switch (opcion_menu) {
			case 0:
				GatosService.verGatos();
				break;

			default:
				break;
			}

		} while (opcion_menu != 1);
	}

}
