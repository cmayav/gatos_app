package gatos_app;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Gatos {

	String id;
	String url;
	String apikey="";
	String image;
}
